ANDROID_NDK=""
ANDROID_SDK=""

if [ -a "env.txt" ]
then
	. env.txt
else
  zenity --error --text="Стой где стоишь!\nlove2d не соберётся без андроидного SDK и NDK!\n\n(Не переживай, сейчас всё это можно будет настроить)"
  echo "Где у тебя там SDK? (например /home/<username>/sdk)"  
  ANDROID_SDK=$(zenity --file-selection --directory --title="Где там у тебя Андроидное SDK? (например /home/<username>/ndk)")
  ANDROID_NDK=$(zenity --file-selection --directory --title="Та-ак, а где у тебя NDK? (например /home/<username>/ndk)")
  echo "ANDROID_HOME=$ANDROID_SDK" > env.txt
  echo "ANDROID_SDK=$ANDROID_SDK" >> env.txt
  echo "ANDROID_NDK=$ANDROID_NDK" >> env.txt

fi

export ANDROID_HOME=$ANDROID_SDK
export ANDROID_SDK=$ANDROID_SDK
export ANDROID_NDK=$ANDROID_NDK
export PATH=${PATH}:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools:$ANDROID_NDK
export

ndk-build -j4

ant release
