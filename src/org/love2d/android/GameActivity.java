package org.love2d.android;

import org.libsdl.app.SDLActivity;

import java.util.List;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.ResultReceiver;
import android.os.Vibrator;
import android.util.Log;
import android.util.DisplayMetrics;
import android.widget.Toast;
import android.view.*;
import android.content.pm.PackageManager;
///diff
import com.google.android.gms.ads.*;
import android.widget.RelativeLayout;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
///

public class GameActivity extends SDLActivity {
    private static DisplayMetrics metrics = new DisplayMetrics();
    private static String gamePath = "";
    private static Context context;
    private static Vibrator vibrator = null;
    private static boolean immersiveActive = false;

    ///diff
    private AdView adView;
    private InterstitialAd mInterstitialAd;
    private AdRequest adRequest;
    private boolean adLoaded = false;
    private boolean hasBanner = false;
    private boolean hasInterstitial = false;
    ///

		@Override 
		protected String[] getLibraries() {
			return new String[] {
       "gnustl_shared",
       "mpg123",
       "openal",
       "love",
			};
		}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      Log.d("GameActivity", "started");
 
      context = this.getApplicationContext();
			
			String permission = "android.permission.VIBRATE";
			int res = context.checkCallingOrSelfPermission(permission);
			if (res == PackageManager.PERMISSION_GRANTED) {
	      vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			} else {
				Log.d("GameActivity", "Vibration disabled: could not get vibration permission.");
			}

      handleIntent (this.getIntent());

      super.onCreate(savedInstanceState);
      getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    /// diff
    private boolean isOnline() {
     ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
     NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
     return activeNetworkInfo != null;
    }

    public void createBanner(final String ad_id, final String side) {
      Log.d("GameActivity", "Calling CreateBanner");

      runOnUiThread(new Runnable(){
        @Override
        public void run() { 

          if (isOnline() && hasBanner == false) {
            adView = new AdView(mSingleton);
            adView.setAdSize(AdSize.BANNER);
            adView.setAdUnitId(ad_id);
            AdSize adSize = adView.getAdSize();

            //small hackish fix for the banner position
            adView.setX((metrics.widthPixels/2) - (adSize.getWidthInPixels(context)/2));
            if (side.trim().equals("bottom")) {
              adView.setY(metrics.heightPixels - adSize.getHeightInPixels(context));
            }

            adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
            mLayout.addView(adView);

            // solution to a strange bug that make the banner disapear
            adView.setAdListener(new AdListener(){
              @Override
              public void onAdLoaded() {
                adView.setVisibility(View.GONE);
                adView.setVisibility(View.VISIBLE);
              }
            });
            hasBanner = true;
            Log.d("GameActivity", "Banner Created. ID: "+ad_id);
          }
        }
      });
    }

    public void createInterstitial(final String ad_id) {
      Log.d("GameActivity", "Calling CreateInterstitial");

      runOnUiThread(new Runnable(){
        @Override
        public void run() { 

          if (isOnline() && hasInterstitial == false) {
            mInterstitialAd = new InterstitialAd(mSingleton);
            mInterstitialAd.setAdUnitId(ad_id);

            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);

            mInterstitialAd.setAdListener(new AdListener() {
              @Override
              public void onAdClosed() {
                AdRequest adRequest = new AdRequest.Builder().build();
                mInterstitialAd.loadAd(adRequest);
              }
            });

            hasInterstitial = true;
            Log.d("GameActivity", "Interstitial Created. ID: "+ad_id);
          }
        }
      });
    }

    public void hideBanner() {
      Log.d("GameActivity", "Calling hideBanner");

      runOnUiThread(new Runnable(){
        @Override
        public void run() { 

          if (hasBanner == true){
            adView.setVisibility(View.GONE);
            Log.d("GameActivity", "Banner Hidden");
          }
        }
      });
    }

    public void showBanner() {
      Log.d("GameActivity", "Calling showBanner");

      runOnUiThread(new Runnable(){
        @Override
        public void run() { 

          if (hasBanner == true){
            adView.loadAd(adRequest);
            adView.setVisibility(View.VISIBLE);
            Log.d("GameActivity", "Banner Showing");
          }
        }
      });
    }

    public boolean isInterstitialLoaded() {
      Log.d("GameActivity", "Calling isInterstitialLoaded");

      runOnUiThread(new Runnable(){
        @Override
        public void run() {

          if (hasInterstitial == true){
            if (mInterstitialAd.isLoaded()) {
              adLoaded = true;
              Log.d("GameActivity", "Ad is loaded");
            }else{
              adLoaded = false;
              Log.d("GameActivity", "Ad is not loaded.");
            }
          }
        }
      });
     return adLoaded;
    }
   
    public void showInterstitial() {
      Log.d("GameActivity", "Calling showInterstitial");

      runOnUiThread(new Runnable(){
        @Override
        public void run(){

          if (hasInterstitial == true){
            if (mInterstitialAd.isLoaded()){
              mInterstitialAd.show();
              Log.d("GameActivity", "Ad loaded!, showing...");
            } else {
              Log.d("GameActivity", "Ad is NOT loaded!, skipping.");
            }
          }
        }
      });
    }
    ///

    @Override
    protected void onNewIntent (Intent intent) {
      Log.d("GameActivity", "onNewIntent() with " + intent);
      handleIntent (intent);
      resetNative();
      startNative();
    };

    protected void handleIntent (Intent intent) {
      Uri game = intent.getData();
      if (game != null) {
        if (game.getScheme().equals ("file")) {
          Log.d("GameActivity", "Received intent with path: " + game.getPath());

          // If we were given the path of a main.lua then use its
          // directory. Otherwise use full path.
          List<String> path_segments = game.getPathSegments();
          if (path_segments.get(path_segments.size() - 1).equals("main.lua")) {
            gamePath = game.getPath().substring(0, game.getPath().length() - "main.lua".length());
          } else {
            gamePath = game.getPath();
          }
        } else {
          copyGameToCache (game);
        }

        Log.d("GameActivity", "new gamePath: " + gamePath);
      }
    };

    @Override
    protected void onDestroy() {
			if (vibrator != null) {
				Log.d("GameActivity", "Cancelling vibration");
				vibrator.cancel();
			}
      super.onDestroy();

      ///diff
      if (hasBanner == true) {
        adView.destroy();
      }
      ///
    }

    @Override
    protected void onPause() {
			if (vibrator != null) {
				Log.d("GameActivity", "Cancelling vibration");
				vibrator.cancel();
			}
      super.onPause();

      ///diff
      if (hasBanner == true) {
        adView.pause();
      }
      ///
    }

    @Override
    public void onResume() {
      super.onResume();

      if (immersiveActive) {
        getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
      }

      ///diff
      if (hasBanner == true) {
        adView.resume();
      }
      ///
    }

    public void setImmersiveMode (boolean immersive_mode) {
			if (android.os.Build.VERSION.SDK_INT < 11) {
				// The API getWindow().getDecorView().setSystemUiVisibility() was
				// added in Android 11 (a.k.a. Honeycomb, a.k.a. 3.0.x). If we run
				// on this we do nothing.
				return;
			}

      immersiveActive = immersive_mode;

      final Object lock = new Object();
      final boolean immersive_enabled = immersive_mode;
      synchronized (lock) {
        runOnUiThread (new Runnable() {
          @Override
          public void run() {
            synchronized (lock) {
              if (immersive_enabled) {
                getWindow().getDecorView().setSystemUiVisibility(
                  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                  | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                  | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                  | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                  | View.SYSTEM_UI_FLAG_FULLSCREEN
                  | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
              } else {
                getWindow().getDecorView().setSystemUiVisibility(
                  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                  | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                  );
              }

              lock.notify();
            }
          }
        });
      };
    }

    public boolean getImmersiveMode () {
      return immersiveActive;
    }

    public static String getGamePath() {
      Log.d ("GameActivity", "called getGamePath(), game path = " + gamePath);
      return gamePath;
    }

    public static DisplayMetrics getMetrics() {
        return metrics;
    }

    public static void vibrate (double seconds) {
			if (vibrator != null) {
				vibrator.vibrate((long) (seconds * 1000.));
			}
    }

    public static void openURL (String url) {
      Log.d ("GameActivity", "opening url = " + url);
      Intent i = new Intent(Intent.ACTION_VIEW);
      i.setData(Uri.parse(url));
      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(i);
    }
  
    void copyGameToCache (Uri sourceuri)
    {
      String destinationFilename = this.getCacheDir().getPath()+"/downloaded.love";
      gamePath = destinationFilename;

      BufferedOutputStream bos = null;
      try {
        bos = new BufferedOutputStream(new FileOutputStream(destinationFilename, false));
      } catch (IOException e) {
        Log.d ("GameActivity", "Could not open destination file:" + e.getMessage());
      }

      int chunk_read = 0;
      int bytes_written = 0;

      BufferedInputStream bis = null;
      if (sourceuri.getScheme().equals("content")) {
        try {
          bis = new BufferedInputStream(getContentResolver().openInputStream(sourceuri));
        } catch (IOException e) {
          Log.d ("GameActivity", "Could not open game file:" + e.getMessage());
        }
      } else {
        Log.d ("GameActivity", "Unsupported scheme: " + sourceuri.getScheme());
      }

      if (bis != null) {
        // actual copying
        try {
          byte[] buf = new byte[1024];
          chunk_read = bis.read(buf);
          do {
            bos.write(buf, 0, chunk_read);
            bytes_written += chunk_read;
            chunk_read = bis.read(buf);        
          } while(chunk_read != -1);
        } catch (IOException e) {
          Log.d ("GameActivity", "Copying failed:" + e.getMessage());
        } 
      }

      // close streams
      try {
        if (bis != null) bis.close();
        if (bos != null) bos.close();
      } catch (IOException e) {
        Log.d ("GameActivity", "Copying failed: " + e.getMessage());
      }

      Log.d("GameActivity", "Copied " + bytes_written + " bytes");
    }
}
